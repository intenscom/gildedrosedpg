package com.gildedrose.rules;

import java.util.function.Predicate;

import com.gildedrose.Item;

public class DoNothingRule implements Rule {

	public int by() {
		return 0;
	}

	public Predicate<Item> condition() {
		return item -> true;
	}
}
