package com.gildedrose.rules;

import java.util.function.Predicate;

import com.gildedrose.Item;

public interface Rule {

	int by();

	Predicate<Item> condition();

	default boolean appliesFor(Item item) {
		return condition().test(item);
	}

}
