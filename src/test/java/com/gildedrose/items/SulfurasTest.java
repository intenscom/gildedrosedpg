package com.gildedrose.items;

import static com.gildedrose.ItemTestHelper.assertItemEquals;

import org.junit.jupiter.api.Test;

import com.gildedrose.GildedRose;
import com.gildedrose.Item;

public class SulfurasTest {

	@Test
	public void sulfurasNeverChanges() {
		GildedRose app = new GildedRose(new Item("Sulfuras, Hand of Ragnaros", 100, 100));

		app.updateQuality();

		assertItemEquals(app.getItems()[0], new Item("Sulfuras, Hand of Ragnaros", 100, 100));
	}
}