package com.gildedrose.items;

import static com.gildedrose.ItemTestHelper.assertItemEquals;

import org.junit.jupiter.api.Test;

import com.gildedrose.GildedRose;
import com.gildedrose.Item;

public class ConjuredItemTest {

	@Test
	public void decreasesInQualityTwiceTheSpeed() {

		GildedRose app = new GildedRose(new Item("Conjured Mana Cake", 3, 6));

		app.updateQuality();

		assertItemEquals(app.getItems()[0], new Item("Conjured Mana Cake", 2, 4));
	}

	@Test
	public void decreasesInQualityTwiceTheSpeedAlsoWhenSellInExpired() {

		GildedRose app = new GildedRose(new Item("Conjured Mana Cake", 0, 6));

		app.updateQuality();

		assertItemEquals(app.getItems()[0], new Item("Conjured Mana Cake", -1, 2));
	}
}